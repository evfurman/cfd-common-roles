---
- name: Get prerequisites
  apt: pkg={{item}} state=installed
  with_items:
    - git
    - python
    - openssl
    - curl
  tags: letsencrypt

- name: Get acme-tiny
  git: >
    repo=https://github.com/diafygi/acme-tiny.git
    dest={{letsencrypt_acme_tiny_dir}}
  tags: letsencrypt

- name: Create keys dir
  file: >
    path="{{letsencrypt_domain_key_dir}}"
    state=directory
    owner=root
    group=root
    mode=0700
  tags: letsencrypt

- name: Upload account key
  copy: content={{letsencrypt_account_key}} dest={{letsencrypt_account_key_path}}
  tags: letsencrypt

- name: Create domain private key
  shell: openssl genrsa 4096 > {{letsencrypt_private_key}}
  args:
    creates: "{{letsencrypt_private_key}}"
  tags: letsencrypt

- name: Key permissions
  file: >
    path="{{item}}"
    owner="root"
    group="root"
    mode=0600
  with_items:
   - "{{letsencrypt_account_key_path}}"
   - "{{letsencrypt_private_key}}"
  tags: letsencrypt

- name: Check if certificate already exists.
  stat: path="{{letsencrypt_signed_cert}}"
  register: existing_cert
  tags: letsencrypt

- name: If cert exists, look up which domains it supports.
  when: existing_cert.stat.exists
  shell: openssl x509 -in "{{letsencrypt_signed_cert}}" -text -noout -certopt no_subject,no_header,no_version,no_serial,no_signame,no_validity,no_subject,no_issuer,no_pubkey,no_sigdump,no_aux | grep DNS | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' -e 's/DNS://g' -e 's/, /\n/g' | sort -V
  register: existing_cert_domains
  changed_when: False
  tags: letsencrypt

- name: Remove current cert if domains don't match
  file: path="{{item}}" state=absent
  with_items:
    - "{{letsencrypt_signed_cert}}"
    - "{{letsencrypt_intermediate_cert}}"
    - "{{letsencrypt_chain}}"
    - "{{letsencrypt_csr}}"
  when: existing_cert.stat.exists and existing_cert_domains.stdout != "{{letsencrypt_domains|sort|join('\n')}}"
  tags: letsencrypt

- name: Create certificate signing request
  shell: "
    openssl req
      -new
      -sha256
      -key {{letsencrypt_private_key}}
      -subj \"/\"
      -reqexts SAN
      -config <(
        cat /etc/ssl/openssl.cnf <(
          printf \"[SAN]\nsubjectAltName=DNS:{{letsencrypt_domains|join(',DNS:')}}\"
        )
      )
      > {{letsencrypt_csr}}"
  args:
    creates: "{{letsencrypt_csr}}"
    executable: /bin/bash
  tags: letsencrypt

- name: Create challenges folder
  file: >
    path="{{letsencrypt_challenges_dir}}"
    state=directory
    recurse=yes
    owner="www-data"
    group="www-data"
    mode=755
  tags:
   - letsencrypt
   - letsencrypt-nginx
   - letsencrypt-apache

- name: Get signed certificate
  shell: "
    t=$(mktemp) ;
    python {{letsencrypt_acme_tiny_dir}}acme_tiny.py
      --account-key {{letsencrypt_account_key_path}}
      --csr {{letsencrypt_csr}}
      --acme-dir {{letsencrypt_challenges_dir}}
      > $t && mv $t {{letsencrypt_signed_cert}}"
  args:
    creates: "{{letsencrypt_signed_cert}}"
  tags: letsencrypt

- name: Get intermediate
  get_url: >
    url=https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem
    dest="{{letsencrypt_intermediate_cert}}"
  register: register_intermediate
  tags: letsencrypt

- name: Remove existing chain if intermediate changed
  file: path={{letsencrypt_chain}} state=absent
  when: register_intermediate.changed
  tags: letsencrypt

- name: Create chain
  become: yes
  become_user: root
  shell: cat {{letsencrypt_signed_cert}} {{letsencrypt_intermediate_cert}} > {{letsencrypt_chain}}
  args:
    creates: "{{letsencrypt_chain}}"
  notify:
   - restart letsencrypt_webserver
  tags: letsencrypt

- name: Letsencrypt cron
  cron: "
    name='letsencrypt renew'
    state=present
    month='*'
    day='2'
    hour='11'
    minute='23' 
    job='python {{letsencrypt_acme_tiny_dir}}acme_tiny.py
           --account-key {{letsencrypt_account_key_path}}
           --csr {{letsencrypt_csr}}
           --acme-dir {{letsencrypt_challenges_dir}}
           > /root/le_temp_signed.crt &&
         curl -L {{letsencrypt_intermediate_url}} > /root/le_temp_intermediate.pem &&
         mv /root/le_temp_signed.crt {{letsencrypt_signed_cert}} &&
         mv /root/le_temp_intermediate.pem {{letsencrypt_intermediate_cert}} &&
         cat {{letsencrypt_signed_cert}} {{letsencrypt_intermediate_cert}}
           > {{letsencrypt_chain}} &&
         service nginx restart || service apache2 restart'"
  tags:
   - letsencrypt
   - letsencrypt-cron
